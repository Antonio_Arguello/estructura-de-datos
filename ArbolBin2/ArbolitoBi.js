class ArbolitoBi
{
    constructor()
    {
        this.padre=this.agregarnodopadre();
    }

    agregarnodo(tipo,valor,nivel,padre,HijoIzq,HijoDer,Identificacion)
    {
        var nodo1=new Nodo(tipo,valor,nivel,padre,HijoIzq,HijoDer,Identificacion);
        return nodo1;
    }

    agregarnodopadre(tipo,valor,nivel,padre,HijoIzq,HijoDer,Identificacion)
    {
        var nodo2= new Nodo(tipo,valor,nivel,padre,HijoIzq,HijoDer,Identificacion);
        return nodo2;
    }
}