class ARBOL
{
    constructor()
    {
        this.NDOPADRE=this.nodopadre;
        this.nivel=2;
        this.BSNIVEL=[];
        this.elemento;
        this.camino='';
        this.SVAL=0;
    }

    agregarnodo(nombre,valor,nivel,hijoIZ,hijoDE,val,padre)
    {
        var NDO=new NODO(nombre,valor,nivel,hijoIZ,hijoDE,val,padre)
        return NDO
    }

    nodopadre(nombre,valor,nivel,hijoIZ,hijoDE,val,padre)
    {
        var NDO=new NODO(nombre,valor,nivel,hijoIZ,hijoDE,val,padre);
        return NDO;
    }

    SUUMNODO(nodo)
    {
        if(nodo.padre!=null)
        {

            this.SVAL=this.SVAL +nodo.padre.val;
            this.SUUMNODO(nodo.padre)
        }
        return this.elemento.val + this.SVAL;

    }

    NIVNODO(nodo)
    {
        if(nodo.nivel==this.nivel)
            this.BSNIVEL.push(nodo.val);

        if(nodo.hasOwnProperty('hijoIZ'))
            this.NIVNODO(nodo.hijoIZ);

        if(nodo.hasOwnProperty('hijoDE'))
            this.NIVNODO(nodo.hijoDE);

        return this.BSNIVEL;
    }

    BUUSNODO(elemento, nodo)
    {
        if(nodo.val==elemento)
            this.elemento=nodo;

        if(nodo.hasOwnProperty('hijoIZ'))
            this.BUUSNODO(elemento,nodo.hijoIZ);

        if(nodo.hasOwnProperty('hijoDE'))
            this.BUUSNODO(elemento,nodo.hijoDE);

        return this.elemento;
    }

    CAMNODO(nodo)
    {
        if(nodo.padre!=null)
        {

            this.camino=this.camino + ' ' +nodo.padre.nombre;
            this.CAMNODO(nodo.padre)
        }
        return this.elemento.nombre + ' ' + this.camino;
    }



}