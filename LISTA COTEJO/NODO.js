class NODO
{
    constructor(nombre,valor, nivel, hijoIZ, hijoDE,val,padre=null)
    {
        this.nombre=nombre;
        this.valor=valor;
        this.nivel=nivel;
        this.hijoIZ=hijoIZ;
        this.hijoDE=hijoDE;
        this.val=val;
        this.padre=padre;
    }

    hijoIZ(nombre,valor,nivel, hijoIZ, hijoDE,val,padre)
    {
        var NDO= new NODO(nombre,valor,nivel,hijoIZ,hijoDE,val,padre)
        return NDO
    }

    hijoDE(nombre,valor,nivel, hijoIZ, hijoDE,val,padre)
    {
        var NDO= new NODO(nombre,valor,nivel,hijoIZ,hijoDE,val,padre)
        return NDO
    }
}